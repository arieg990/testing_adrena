var express = require('express');
var router = express.Router();
var model = require("../models")
var moment = require("moment")
var sequelize = require("sequelize")
var passportAuth = require("../config/passportAuth")
var Op = sequelize.Op
var response = require("../config/constant").response;
const { isAdmin } = require('../config/passportAuth');
const { query } = require('express');
const check = require("../middleware/routerMiddleware")

async function ownData(req, res, next) {
  try {
    const data = await model.events.count({
      where: {
        id: req.params.id,
        adminId: req.user.id
      }
    })

    if (data < 1) {
      return res.status(200).json(response(404, "event", {
        "message": "Data not found",
        "path": "events",
        "value": null
      }))
    } else {
      return next()
    }
  } catch (error) {
    return res.status(200).json(response(404, "event", error))
  }
}

/* GET home page. */
router.get('/', async function (req, res, next) {
  var page = 0;
  var perPage = 10;
  var offset = parseInt(req.query.page)
  var limit = parseInt(req.query.limit)
  var where = {}
  var showData = req.query.showData
  const eventStatus = req.query.status

  if (offset > 1) {
    page = offset - 1
  }

  if (limit >= 1) {
    perPage = limit
  }

  if (eventStatus == "upcoming") {
    where.eventStart = {
      [Op.gt]: moment()
    }
  } else if (eventStatus == "ended") {
    where.eventEnd = {
      [Op.lt]: moment()
    }
  }

  if (!showData) {
    where.isDeleted = false
  } else if (showData == "delete") {
    where.isDeleted = true
  }

  try {
    const events = await model.events.findAll({
      limit: perPage,
      offset: page * perPage,
      where,
      order: [
        ["id", "ASC"]
      ],
      attributes: {
        include: [
          [sequelize.literal('"admin"."fullName"'), 'adminName'],
        ]
      },
      include: [
        {
          model: model.admin,
          as: 'admin',
          attributes: []
        }
      ]
    })

    if (events.length > 0) {

      var count = await model.events.count({
        where: where
      })

      var totalPage = Math.ceil(count / perPage)

      var paging = {
        "currentPage": page + 1,
        "limitPerPage": perPage,
        "totalPage": totalPage
      }

      return res.status(200).json(response(200, "events", events, paging))
    } else {
      return res.status(200).json(response(404, "events", {
        "message": "Data not found",
        "path": "events",
        "value": null
      }))
    }
  } catch (error) {
    return res.status(200).json(response(400, "events", error))
  }
});

router.get("/:id", async function (req, res, next) {

  try {
    var include = [
      {
        model: model.admin,
        as: 'admin',
        attributes: []
      },
      {
        model: model.audience,
        as: 'audiences',
        required: false,
        attributes:{
          include: [
            [sequelize.literal('"audiences->customer"."fullName"'), 'customerName'],
          ],
        },
        include: [
          {
            model: model.customer,
            as: 'customer',
            attributes: []
          }
        ]
      }
    ]

    const event = await model.events.findOne({
      where: {
        id: req.params.id,
        isDeleted: req.query.isDeleted ? req.query.isDeleted : false
      },
      attributes: {
        include: [
          [sequelize.literal('"admin"."fullName"'), 'adminName'],
        ]
      },
      include
    })

    if (!event) {
      return res.status(200).json(response(404, "event", {
        "message": "Data not found",
        "path": "events",
        "value": null
      }))
    } else {
      return res.status(200).json(response(200, "event", event))
    }

  } catch (error) {
    return res.status(200).json(response(400, "event", error))
  }
})

router.post("/",
  passportAuth.isAdmin,
  check.removeTimeStampcreateUpdate,
  async function (req, res, next) {
    try {
      var data = req.body
      data.adminId = req.user.id


      const event = await model.events.create(req.body)

      return res.status(200).json(response(200, "event", event))
    } catch (error) {
      return res.status(200).json(response(400, "event", error))
    }
  })

router.delete("/:id",
  passportAuth.isAdmin,
  ownData,
  check.removeTimeStampcreateUpdate,
  async function (req, res, next) {
    try {

      const id = req.params.id
      const event = await model.events.update({
        isDeleted: true
      }, {
        where: {
          id
        }
      })

      if (event[0] == 1) {
        return res.status(200).json(response(200, "event", event[0]))
      } else {
        return res.status(200).json(response(200, "event", {
          "message": "Delete Event Failed",
          "path": null,
          "value": null
        }))
      }
    } catch (error) {
      return res.status(200).json(response(400, "event", error))
    }
  })

router.put("/changeDeleteStatus/:id",
  passportAuth.isAdmin,
  ownData,
  check.removeTimeStampcreateUpdate,
  async function (req, res, next) {
    try {
      const id = req.params.id

      const event = await model.events.update({
        isDeleted: false
      }, {
        where: {
          id
        }
      })

      if (event[0] == 1) {
        return res.status(200).json(response(200, "event", event[0]))
      } else {
        return res.status(200).json(response(200, "event", {
          "message": "Change delete status Event Failed",
          "path": null,
          "value": null
        }))
      }
    } catch (error) {
      return res.status(200).json(response(400, "event", error))
    }
  })

router.put("/:id",
  passportAuth.isAdmin,
  check.removeTimeStampcreateUpdate,
  async function (req, res, next) {
    try {
      var data = req.body
      var id = req.params.id
      data.adminId = req.user.id
      delete data.createdId
      delete data.updatedId

      const update = await model.events.update(req.body, {
        where: {
          id
        }
      })

      if (update[0] == 1) {
        const event = await model.events.findByPk(id)
        return res.status(200).json(response(200, "event", event))
      } else {
        return res.status(200).json(response(200, "event", {
          "message": "Update Event Failed",
          "path": null,
          "value": null
        }))
      }

    } catch (error) {
      return res.status(200).json(response(400, "event", error))
    }
  })

module.exports = router;
