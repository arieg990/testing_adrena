var express = require('express');
var router = express.Router();
var model = require("../models")
var response = require("../config/constant").response;

router.post("/:id", async function(req, res, next) {
  if (req.user.type == "admin") {
    return res.status(200).json(response(400, "audience", {
      "message": "Cannot Register event",
      "path": "events",
      "value": null
    }))
  } else {
    try {
      const data = {
        eventId:req.params.id,
        customerId: req.user.id
      }
      const audienceCount = await model.audience.count({
        where: data
      })

      if (audienceCount > 0) {
        return res.status(200).json(response(400, "audience", {
          "message": "Already register this event",
          "path": "events",
          "value": null
        }))
      } else {
        const create = await model.audience.create(data)

      return res.status(200).json(response(200, "audience", create))
      }
    } catch (error) {
      return res.status(200).json(response(400, "audience", error))
    }
  }
})

module.exports = router;