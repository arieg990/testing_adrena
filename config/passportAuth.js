const LocalStrategy = require('passport-local').Strategy;
const BearerStrategy = require("passport-http-bearer").Strategy;
const jwt = require("jsonwebtoken")
const model = require("../models")
const passport = require("passport")
const response = require("./constant").response

function local(passport) {
  passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
  },
    async function (req, username, password, done) {

      if (typeof req.body.type === "undefined") {
        return done(null, false, {
          "message": "Type required.",
          "path": "type",
          "value": null
        })
      }

      if (req.body.type == "admin") {
        try {
          const admin = await model.admin.findOne({
            where: {
              username: username
            }
          });

          if (admin.validPassword(password)) {
            var token = jwt.sign({ id: admin.id, type: "admin" }, "data_testing", { expiresIn: '1h' })

            admin.type = "admin"
            delete admin.get().password

            const resp = {
              user: admin,
              token: token
            }

            return done(null, resp)
          }
        } catch (error) {
          return done(null, false, {
            "message": "Account not found",
            "path": "username",
            "value": null
          })
        }
      } else {
        try {
          const customer = await model.customer.findOne({
            where: {
              username: username
            }
          });

          if (customer.validPassword(password)) {
            var token = jwt.sign({ id: customer.id, type: "customer" }, "data_testing", { expiresIn: '1h' })

            customer.type = "customer"
            delete customer.get().password

            const resp = {
              user: customer,
              token: token
            }

            return done(null, resp)
          }
        } catch (error) {
          console.log(error);
          return done(null, false, {
            "message": "Account not found",
            "path": "username",
            "value": null
          })
        }
      }
    }
  ))
}

function bearer(passport) {
  passport.use(new BearerStrategy(
    async function (token, done) {
      try {
        var data = jwt.verify(token, "data_testing");
      if (data) {
          return done(null, data)
        } else {
          return done(null, false, {
            "message": "Token expired.",
            "path": "authorization",
            "value": token
          })
        }
      } catch (error) {
        return done(null, false, {
          "message": "Token expired.",
          "path": "authorization",
          "value": token
        })
      }
    }
  ))
}

function isLoggedIn(req, res, next) {

  passport.authenticate('bearer', { session: false }, function (err, user, info) {
    if (err) { return res.status(200).json(response(401, "unauthorized", null)); }
    if (!user) { return res.status(200).json(response(401, "unauthorized", null)); }

    req.user = user

    return next();
  })(req, res, next)

}

function isAdmin(req, res, next) {
  if (req.user.type == "admin") {
    return next();
  } else {
    return res.status(200).json(response(401, "restricted", null));
  }
}

module.exports = { local, isLoggedIn, isAdmin, bearer }