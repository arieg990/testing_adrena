'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('events','isDeleted', {
      type: Sequelize.BOOLEAN,
      after:"adminId",
    });
  }
};