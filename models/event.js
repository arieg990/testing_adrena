module.exports = (sequelize, DataTypes) => {
  const event = sequelize.define("events", {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "title cannot be empty"
        }
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Description cannot be empty"
        }
      }
    },
    eventStart: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Event Start cannot be empty"
        }
      }
    },
    eventEnd: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Event End cannot be empty"
        }
      }
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      default: false
    },
    price: {
      type: DataTypes.DOUBLE(13, 4),
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Event End cannot be empty"
        }
      }
    },
  }, {
    tableName: "events",
    freezeTableName: true,
  });

  event.associate = function (models) {

    event.belongsTo(models.admin, {
      foreignKey: {
        name: 'adminId',
        allowNull: false
      },
      onUpdate: 'CASCADE',
      as: "admin"
    });

    event.hasMany(models.audience, {
      foreignKey: {
        name: 'eventId',
        allowNull: false
      },
      onUpdate: 'CASCADE',
      as: "audiences"
    })
  }

  return event;
}