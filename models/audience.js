module.exports = (sequelize, DataTypes) => {
  const audience = sequelize.define("audience", {}, {
    tableName: "audiences",
    freezeTableName: true,
  });

  audience.associate = function (models) {

    audience.belongsTo(models.events, {
      foreignKey: {
        name: 'eventId',
        allowNull: false,
        primaryKey: true,
      },
      onUpdate: 'CASCADE',
      as: "event"
    });

    audience.belongsTo(models.customer, {
      foreignKey: {
        name: 'customerId',
        allowNull: false,
        primaryKey: true,
      },
      onUpdate: 'CASCADE',
      as: "customer"
    });
  }

  audience.removeAttribute('id');

  return audience;
}