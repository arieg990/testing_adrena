function removeTimeStampcreateUpdate(req, res, next) {
  delete req.body.createdAt
  delete req.body.updatedAt
  return next()
}

module.exports = {removeTimeStampcreateUpdate}